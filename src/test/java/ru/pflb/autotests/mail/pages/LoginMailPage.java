package ru.pflb.autotests.mail.pages;

import org.openqa.selenium.WebElement;

public class LoginMailPage extends BasePage{
    public void enterLogin(String login){
        switchToLoginFrame();
        WebElement loginField = driver.findElementByXPath("//input[@name='Login']");
        loginField.sendKeys(login);
        driver.switchToParentFrame();
    }
    public void enterPassword(String pass){
        switchToLoginFrame();
        WebElement passwordField = driver.findElementByXPath("//input[@name='Password']");
        passwordField.sendKeys(pass);
        driver.switchToParentFrame();
    }

    private void switchToLoginFrame(){
        String parentIframe = "//iframe[@class='ag-popup__frame__layout__iframe']";
        String childIframe = "//iframe[@class='c0113']";
        driver.switchToFrame(parentIframe);
    }
}
