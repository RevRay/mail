package ru.pflb.autotests.mail.pages;

import org.openqa.selenium.WebElement;

public class MainPage extends BasePage {
    public void open(){
        driver.get("http://www.mail.ru");
    }
    public void clickMail(){
        WebElement mail = driver.findElementByXPath("//a[@id='ph_mail']");
        mail.click();
    }
}
